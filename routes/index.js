var express = require('express');
var router = express.Router();


router.get('/', function(req, res, next) {

    var wrong_number = req.session.wrong_number;
    //verifier si la variable wrong_number existe dans la session
    if (wrong_number == 401) {
        req.session.destroy()
        res.render('index', { error: 'Entrez un nombre à quatre chiffres' });
    } else {
        res.render('index', { error: false });
    }

});

module.exports = router;