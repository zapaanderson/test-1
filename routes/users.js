var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/year', function(req, res, next) {

    //récupération de la valeur du formulaire
    var year = req.body.year;

    //Verifier si la valeur est un nombre à quatre chiffres
    if (isNaN(year) || year.length != 4) {
        req.session.wrong_number = 401;
        res.redirect('/');
    }

    var nextYear = new Date().getFullYear() + 1;
    var ageNextYear = nextYear - year;
    res.render('result', { ageNextYear: ageNextYear, nextYear: nextYear });

});

module.exports = router;